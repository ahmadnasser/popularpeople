package com.popularpeople.viewmodels

import android.content.Context
import android.net.Uri
import android.os.Environment
import android.os.Environment.getExternalStorageDirectory
import androidx.lifecycle.*
import com.popularpeople.constants.IMAGES_BASE_URL
import com.popularpeople.model.PopularResponse
import com.popularpeople.model.repository.TheMovieRepository
import com.popularpeople.utils.FileUtils
import com.popularpeople.utils.LoadingState
import kotlinx.coroutines.launch
import okhttp3.ResponseBody
import java.io.*


class PopularPeopleViewModel(private val theMovieRepository: TheMovieRepository) : ViewModel() {
    private val _loadingState = MutableLiveData<LoadingState>()
    val loadingState: LiveData<LoadingState>
        get() = _loadingState

    private val _downloadingImageState = MutableLiveData<LoadingState>()
    val downloadingImageState: LiveData<LoadingState>
        get() = _downloadingImageState


    var popularPeopleList = MutableLiveData<PopularResponse>()
    var actorImage = MutableLiveData<Uri>()


    fun getPopularPeople(page: Int) {

        viewModelScope.launch {

            try {
                _loadingState.value = LoadingState.LOADING
                popularPeopleList.value = theMovieRepository.getPopularPeople(page)
                _loadingState.value = LoadingState.LOADED
            } catch (e: Exception) {
                _loadingState.value = LoadingState.error(e.message)
            }

        }
    }

    fun downloadImage(url:String){
        var fullPath= IMAGES_BASE_URL+url
        viewModelScope.launch {
            try {
                _downloadingImageState.value= LoadingState.LOADING
                var responseBody =
                    theMovieRepository.downloadPopularPeopleImage(fullPath)
                responseBody?.let {
                    var uri = FileUtils.writeResponseBodyToDisk(it,fullPath.substringAfterLast("/"))
                    actorImage.value = uri
                    _downloadingImageState.value= LoadingState.LOADED
                }
            } catch (e: Exception) {
                e.printStackTrace()
                _downloadingImageState.value= LoadingState.error(e.message)
            }
        }
    }











}