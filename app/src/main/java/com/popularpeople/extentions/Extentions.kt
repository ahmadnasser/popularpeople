package com.popularpeople.extentions

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.View
import android.widget.ImageView
import com.popularpeople.R
import com.popularpeople.constants.IMAGES_BASE_URL
import com.popularpeople.utils.CircleTransform
import com.squareup.picasso.Picasso
import java.io.File
import java.io.IOException
import java.net.URL


fun View.gone() {
    visibility = View.GONE
}
fun View.invisible() {
    visibility = View.INVISIBLE
}
fun View.show() {
    visibility = View.VISIBLE
}


fun ImageView.showImageFromUrl(url:String,height:Int,width:Int,isCircle:Boolean)
{
    var absolutePath = "$IMAGES_BASE_URL$url"
    when(isCircle)
    {
        true -> {
            Picasso.get().load(absolutePath).placeholder(R.drawable.ic_avatar).error(R.drawable.ic_avatar).resize(height,width).centerCrop().transform( CircleTransform()).into(this)

        }

        false ->{
            Picasso.get().load(absolutePath).placeholder(R.drawable.ic_avatar).error(R.drawable.ic_avatar).resize(height,width).centerCrop().into(this)

        }
    }

}

 val File.extension: String
    get() = name.substringAfterLast('.', "")

fun URL.toBitmap(): Bitmap?{
    return try {
        BitmapFactory.decodeStream(openStream())
    }catch (e: IOException){
        null
    }
}

