package com.popularpeople.utils

import android.net.Uri
import android.os.Environment
import okhttp3.ResponseBody
import java.io.*

class FileUtils{
    companion object{
         fun writeResponseBodyToDisk(body: ResponseBody, fileName:String): Uri? {
             try {

                val rootPath = File(Environment.getExternalStorageDirectory(), "popularPeople")


                if(!rootPath.exists())
                {
                    rootPath.mkdirs()
                }
                val fileToDownload =
                    File(rootPath.absolutePath+"/" + fileName)

                var inputStream: InputStream? = null
                var outputStream: OutputStream? = null
                try {
                    val fileReader = ByteArray(4096)
                    var fileSizeDownloaded: Long = 0
                    inputStream = body.byteStream()
                    outputStream = FileOutputStream(fileToDownload)
                    while (true) {
                        val read: Int = inputStream.read(fileReader)
                        if (read == -1) {
                            break
                        }
                        outputStream.write(fileReader, 0, read)
                        fileSizeDownloaded += read.toLong()

                    }
                    outputStream.flush()
                    return Uri.parse(fileToDownload.toString())
                } catch (e: IOException) {
                    return null
                } finally {
                    inputStream?.close()
                    outputStream?.close()
                }
            } catch (e: IOException) {
                return null
            }
        }
    }
}