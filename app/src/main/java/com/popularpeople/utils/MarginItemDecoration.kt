package com.popularpeople.utils

import android.graphics.Rect

import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * Created on 11/27/2019.
 */
class MarginItemDecoration(private val marginTop: Int,private val marginLeft: Int,private val marginRight: Int,private val marginBottom: Int) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect, view: View,
                                parent: RecyclerView, state: RecyclerView.State) {
        with(outRect) {
            if (parent.getChildAdapterPosition(view) == 0) {
                top = marginTop

            }
            left =  marginLeft
            right = marginRight
            bottom = marginBottom
        }
    }
}
