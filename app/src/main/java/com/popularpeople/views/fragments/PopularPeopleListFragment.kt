package com.popularpeople.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.popularpeople.R
import com.popularpeople.constants.ExtraKeys
import com.popularpeople.extentions.gone
import com.popularpeople.extentions.show
import com.popularpeople.model.Actor
import com.popularpeople.model.LoadMore
import com.popularpeople.utils.EndlessRecyclerViewScrollListener
import com.popularpeople.utils.LoadingState
import com.popularpeople.utils.MarginItemDecoration
import com.popularpeople.viewmodels.PopularPeopleViewModel
import com.popularpeople.views.adapters.PopularPeopleAdapter
import com.popularpeople.views.adapters.onPopularItemClicked
import kotlinx.android.synthetic.main.fragment_popular_people_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class PopularPeopleListFragment : BaseFragment(),onPopularItemClicked {

    private val popularViewModel by viewModel<PopularPeopleViewModel>()
    private var popularPeopleAdapter: PopularPeopleAdapter? = null
    private lateinit var scrollListener: EndlessRecyclerViewScrollListener
    private var isLoadingMore = false
    lateinit var linearLayoutManager: LinearLayoutManager




    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_popular_people_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)



    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        popularViewModel.getPopularPeople(1)
        setupRecycleView()
        setUpSwipeToRefresh()
        setupObservables()

    }

    private fun setupObservables() {

        popularViewModel.popularPeopleList.observe(viewLifecycleOwner, Observer {
            if (isLoadingMore) {
                popularPeopleAdapter?.removeLoadingMoreItem()
            }

            if(loadPopularPb.isVisible)
            {
                loadPopularPb.gone()
            }

            if(swipe_refresh_actors.isRefreshing)
            {
                swipe_refresh_actors.isRefreshing=false
            }

            it.actors?.let { it1 -> popularPeopleAdapter?.appendItems(it1) }
            isLoadingMore = false
        })

    }

    private fun setUpSwipeToRefresh() {
        swipe_refresh_actors.setOnRefreshListener {
            popularPeopleAdapter?.clearDataSource()
            scrollListener.resetState()
            popularViewModel.getPopularPeople(1)

        }
    }

    private fun setupRecycleView() {

        linearLayoutManager = LinearLayoutManager(activity)
        popularPeopleRv.layoutManager = linearLayoutManager
        popularPeopleRv.addItemDecoration(MarginItemDecoration(20, 20, 20, 20))
        popularPeopleAdapter = PopularPeopleAdapter()
        popularPeopleAdapter?.onPopularItemClicked=this
        popularPeopleRv.adapter = popularPeopleAdapter

        scrollListener = object : EndlessRecyclerViewScrollListener(linearLayoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {

                if(!isLoadingMore)
                {
                    popularPeopleAdapter?.addLoadingMoreItem(loadMore = LoadMore("Loading Actors"))
                    popularViewModel.getPopularPeople(page+1)
                    isLoadingMore=true

                }

            }
        }
        popularPeopleRv.addOnScrollListener(scrollListener)
    }


    override fun onItemClicked(actor: Actor, position: Int) {
        val data=Bundle()
        data.putParcelable(ExtraKeys.ACTOR,actor)
        addFragment(PopularPeopleDetailsFragment(),true,getString(R.string.popular_people_details),data)


    }


}

