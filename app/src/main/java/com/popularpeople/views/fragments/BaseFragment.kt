package com.popularpeople.views.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.popularpeople.R

open class BaseFragment:Fragment() {


        fun addFragment(
            fragment: Fragment,
            addToBackStack: Boolean,
            tag: String?,
            data: Bundle?
        ) {
            val transaction =
                requireActivity().supportFragmentManager.beginTransaction()
            fragment.arguments = data
             transaction.setCustomAnimations(
                    R.anim.frag_enter_from_left,
                    R.anim.frag_exit_from_right,
                    R.anim.pop_enter,
                    R.anim.pop_exit
                )
                transaction.add(R.id.frame_fragment_content, fragment)
            if(addToBackStack)
            {
                transaction.addToBackStack(tag)
            }

            transaction.commit()
        }

}