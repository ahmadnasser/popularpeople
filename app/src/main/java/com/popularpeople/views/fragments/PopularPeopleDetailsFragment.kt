package com.popularpeople.views.fragments

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import com.popularpeople.R
import com.popularpeople.constants.ExtraKeys
import com.popularpeople.constants.IMAGES_BASE_URL
import com.popularpeople.model.Actor
import com.popularpeople.utils.LoadingState
import com.popularpeople.viewmodels.PopularPeopleViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_popular_people_details.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions


class PopularPeopleDetailsFragment : BaseFragment(), EasyPermissions.PermissionCallbacks {
    private val popularViewModel by viewModel<PopularPeopleViewModel>()
    var actor: Actor? = null

    

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_popular_people_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            actor = it.getParcelable(ExtraKeys.ACTOR)
        }

        initViews()
        initObservables()




    }

    private fun initObservables() {
        popularViewModel.downloadingImageState.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                LoadingState.Status.FAILED -> Toast.makeText(activity, it.msg, Toast.LENGTH_SHORT)
                    .show()
                LoadingState.Status.RUNNING -> btn_download_image.text = "Downloading..."
                LoadingState.Status.SUCCESS -> btn_download_image.text = "Download"
            }
        })

        popularViewModel.actorImage.observe(viewLifecycleOwner, Observer {
            Toast.makeText(
                activity,
                getString(R.string.downlaod_image_success),
                Toast.LENGTH_SHORT
            ).show()
        })
    }

    private fun initViews() {

        Picasso.get().load(IMAGES_BASE_URL + actor?.image).into(iv_actor_image)
        tv_actor_name.text = actor?.name
        btn_download_image.setOnClickListener {
            downloadImage()
        }
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            PopularPeopleDetailsFragment()
    }

    @AfterPermissionGranted(1)
    private fun downloadImage() {


        if (EasyPermissions.hasPermissions(
                requireContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
        ) {
            actor?.image?.let { imagePath ->
                popularViewModel.downloadImage(imagePath)
            }

        } else {
            EasyPermissions.requestPermissions(
                this@PopularPeopleDetailsFragment,
                getString(R.string.rationale_message),
                1,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
        }
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            AppSettingsDialog.Builder(this).build().show()
        }
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE) {
            downloadImage()
        }
    }
}
