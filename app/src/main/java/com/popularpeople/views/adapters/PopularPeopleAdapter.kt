package com.popularpeople.views.adapters;

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.popularpeople.R
import com.popularpeople.extentions.showImageFromUrl
import com.popularpeople.model.Actor
import com.popularpeople.model.LoadMore
import java.util.ArrayList

enum class ViewTypes(val type:Int){
    ACTOR(1),
    LOAD_MORE(2)
}

interface onPopularItemClicked{
    fun onItemClicked(actor: Actor,position: Int)
}
class PopularPeopleAdapter() :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var actors= ArrayList<Actor>()
    lateinit var context: Context
    var onPopularItemClicked:onPopularItemClicked?=null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        this.context = parent.context
        return if(viewType==ViewTypes.ACTOR.type) {
            ActorViewHolder(
                LayoutInflater.from(context).inflate(R.layout.item_popular_people, parent, false)
            )
        }else{
            LoadMoreViewHolder(
                LayoutInflater.from(context).inflate(R.layout.item_load_more, parent, false)
            )
        }

    }

    fun clearDataSource()
    {
        actors.clear()
    }
    fun addLoadingMoreItem(loadMore: LoadMore)
    {

        var actor=Actor()
        actor.loadMoreText =loadMore.loadingMessage
        actor.isLoader=true
        actors.add(actor)
        notifyItemInserted(actors.size-1)

    }


    fun removeLoadingMoreItem()
    {
        actors.removeAt(actors.size-1)
        notifyItemRemoved(actors.size)

    }
    fun appendItems(arrayList: ArrayList<Actor>)
    {
        actors.addAll(arrayList)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return actors.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        var item = actors[position]

        if(holder is ActorViewHolder)
        {

            holder.actorDepartment.text=item.department
            holder.actorName.text=item.name
            item.image?.let { holder.actorImage.showImageFromUrl(it,150,150,true) }
        }else if(holder is LoadMoreViewHolder){

            holder.loadingMesaage.text=item.loadMoreText
        }

    }

    override fun getItemViewType(position: Int): Int {
        return if(actors[position].isLoader) {
            ViewTypes.LOAD_MORE.type
        }else{
            ViewTypes.ACTOR.type
        }
    }

    inner class ActorViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var actorName = view.findViewById<TextView>(R.id.tv_actor_name)
        var actorDepartment = view.findViewById<TextView>(R.id.tv_actor_department)
        var actorImage = view.findViewById<ImageView>(R.id.iv_actor_image)

        init {
            view.setOnClickListener {
                onPopularItemClicked?.onItemClicked(actors[adapterPosition],adapterPosition)
            }
        }

    }


    inner class LoadMoreViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var loadingMesaage = view.findViewById<TextView>(R.id.tv_load_more_text)

    }
}