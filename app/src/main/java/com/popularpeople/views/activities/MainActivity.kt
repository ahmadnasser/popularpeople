package com.popularpeople.views.activities

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.popularpeople.R
import com.popularpeople.views.fragments.PopularPeopleListFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        supportFragmentManager?.beginTransaction()?.replace(
            R.id.frame_fragment_content,
            PopularPeopleListFragment()
        ).commit()


        supportFragmentManager.addOnBackStackChangedListener {
            if (supportFragmentManager.backStackEntryCount > 0) {
                setToolBarTitle(getActiveFragment())
            } else {
                setupCurrentFragmentTitle()
            }
        }

    }

    private fun setToolBarTitle(title: String?) {
      supportActionBar?.title=title
    }

    private fun getActiveFragment(): String? {
        return if (supportFragmentManager.backStackEntryCount === 0) {
            null
        } else supportFragmentManager.getBackStackEntryAt(supportFragmentManager.backStackEntryCount - 1)
            .name
    }

    private fun setupCurrentFragmentTitle() {
        try {
            val f =
                supportFragmentManager.findFragmentById(R.id.frame_fragment_content)
            if (f == null) {
                setToolBarTitle(getString(R.string.popular_people))

                return
            }

            if(f is PopularPeopleListFragment)
            {
                setToolBarTitle(getString(R.string.popular_people))
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}
