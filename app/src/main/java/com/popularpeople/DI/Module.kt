package com.popularpeople.DI

import android.app.Application
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.popularpeople.constants.BASE_URL
import com.popularpeople.model.api.TheMovieApi
import com.popularpeople.model.repository.TheMovieRepository
import com.popularpeople.viewmodels.PopularPeopleViewModel
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val viewModelModule = module {
    viewModel { PopularPeopleViewModel(theMovieRepository = get()) }
}

val apiModule = module {
    fun provideTheMovieApi(retrofit: Retrofit): TheMovieApi {
        return retrofit.create(TheMovieApi::class.java)
    }

    single { provideTheMovieApi(get()) }
}

val repositoryModule = module {
    fun providePopularRepository(api: TheMovieApi): TheMovieRepository {
        return TheMovieRepository(api)
    }

    single { providePopularRepository(get()) }
}

    val networkModule = module {
        fun provideCache(application: Application): Cache {
            val cacheSize = 10 * 1024 * 1024
            return Cache(application.cacheDir, cacheSize.toLong())
        }

        fun provideHttpClient(cache: Cache): OkHttpClient {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            val okHttpClientBuilder = OkHttpClient.Builder()
                .addInterceptor(logging)
                .cache(cache)

            return okHttpClientBuilder.build()
        }

        fun provideGson(): Gson {
            return Gson()
        }



        fun provideRetrofit(factory: Gson, client: OkHttpClient): Retrofit {

            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(factory))
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .client(client)
                .build()
        }

        single { provideCache(androidApplication()) }
        single { provideHttpClient(get()) }
        single { provideGson() }
        single { provideRetrofit(get(), get()) }


}