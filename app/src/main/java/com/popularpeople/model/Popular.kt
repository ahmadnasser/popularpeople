package com.popularpeople.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class PopularResponse(
    val page:Int?=null,
    @SerializedName("results")
    val actors:ArrayList<Actor>?=null,
    @SerializedName("total_pages")
    val totalPages:Int?=null,
    @SerializedName("total_results")
    val totalResults:Int?=null
)

open  class Actor() :Parcelable {
    val name: String? = null

    @SerializedName("known_for_department")
    val department: String? = null

    @SerializedName("profile_path")
    var image: String? = null


    var loadMoreText:String?=null
    var isLoader:Boolean=false

    constructor(parcel: Parcel) : this() {
        loadMoreText = parcel.readString()
        isLoader = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        p0?.writeString(loadMoreText)
        p0?.writeByte(if (isLoader) 1 else 0)
    }



    companion object CREATOR : Parcelable.Creator<Actor> {
        override fun createFromParcel(parcel: Parcel): Actor {
            return Actor(parcel)
        }

        override fun newArray(size: Int): Array<Actor?> {
            return arrayOfNulls(size)
        }
    }

    override fun describeContents(): Int {
        return 0
    }

}