package com.popularpeople.model.api

import com.popularpeople.model.PopularResponse
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url


interface TheMovieApi {
    @GET("person/popular")
    suspend fun getPopularPeople(@Query("api_key") apiKey:String,
                                 @Query("language") language: String,
                                 @Query("page") page: Int): PopularResponse

    @GET
    suspend fun downloadImageFromUrl(@Url fileUrl: String?):ResponseBody

}