package com.popularpeople.model.repository

import android.graphics.Bitmap
import com.popularpeople.constants.API_KEY
import com.popularpeople.constants.API_LANGUAGE
import com.popularpeople.extentions.toBitmap
import com.popularpeople.model.PopularResponse
import com.popularpeople.model.api.TheMovieApi
import okhttp3.ResponseBody
import java.net.URL

class TheMovieRepository(private val theMovieApi: TheMovieApi) {

    suspend fun getPopularPeople(page: Int): PopularResponse {
        return theMovieApi.getPopularPeople(API_KEY, API_LANGUAGE, page)
    }

     suspend fun downloadPopularPeopleImage(url: String): ResponseBody? {
         return theMovieApi.downloadImageFromUrl(url)

    }
}